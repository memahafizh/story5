from django.db import models
from django.db.models.deletion import CASCADE

# Create your models here.
class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=50, null = True, blank= False)

    class Meta:
        db_table = 'Kegiatan'

    def __str__(self):
        return self.namaKegiatan

class Orang(models.Model):
    namaOrang = models.CharField(max_length=50, null=True, blank=False)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, null = True, blank = False)

    class Meta:
        db_table = 'Orang'

    def __str__(self):
        return self.namaOrang