from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('tambah/', views.tambahK, name='tambahK'),
    path('data/', views.dataK, name='dataK'),
]