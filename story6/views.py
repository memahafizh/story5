from django.http import request
from django.shortcuts import render, redirect
from .models import Orang, Kegiatan
from .forms import formOrang, formKegiatan
# Create your views here.
def landing(request):
    return render(request, 'landing.html')

def tambahK(request):
    form = formKegiatan()
    if form.is_valid and request.method ==  "POST":
        form = formKegiatan(request.POST)
        form.save()
        return redirect('/story6/')
    else:
        context = {'form': form}
        return render(request, 'tambahK.html', context)

def dataK(request):
    form = formOrang()
    dataO = Orang.objects.all()
    dataKe = Kegiatan.objects.all()
    if form.is_valid and request.method ==  "POST":
        form = formOrang(request.POST)
        form.save()
        return redirect('/story6/data')
    else:
        context = {'form': form, 'dataO': dataO, 'dataKe': dataKe}
        return render(request, 'dataK.html', context) 