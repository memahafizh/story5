from django import forms
from django.forms import fields
from .models import Kegiatan, Orang

class formOrang(forms.ModelForm):
    class Meta:
        model = Orang
        fields = '__all__'

class formKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'