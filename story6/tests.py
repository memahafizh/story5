from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from .models import Orang, Kegiatan
from .views import landing, tambahK, dataK
from .forms import formKegiatan, formOrang
from .apps import Story6Config
# Create your tests here.

class Story6Test(TestCase):
    def test_story6app_landing_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_story6app_landing_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_story6app_haltambah_template(self):
        response = Client().get('/story6/tambah/')
        self.assertTemplateUsed(response, 'tambahK.html')  

    def test_story6app_tambah_is_exist(self):
        response = Client().get('/story6/tambah/')
        self.assertEqual(response.status_code, 200)

    def test_story6app_data_is_exist(self):
        response = Client().get('/story6/data/')
        self.assertEqual(response.status_code, 200)

    def testConfig(self):
        self.assertEqual(Story6Config.name, 'story6')
