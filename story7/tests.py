from django.test import TestCase, Client
from .views import story7
from .apps import Story7Config
# Create your tests here.

class Story7(TestCase):

    def page_exist(self):
        response = Client().get("/story7/")
        self.assertEqual(response.status_code,200)

    def testConfig(self):
        self.assertEqual(Story7Config.name, 'story7')
