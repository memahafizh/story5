from django import forms
from .models import Sched

class Jadwal(forms.ModelForm):
    class Meta:
        model = Sched
        fields = '__all__'