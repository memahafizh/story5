from django.shortcuts import render, redirect
from .models import Sched
from .forms import Jadwal

# Create your views here.
def index(request):
    context = {"check" :0}
    return render(request, 'index.html', context)

def exp(request):
    context = {"check" :1}
    return render(request, 'exp.html', context)

def gallery(request):
    context = {"check" :2}
    return render(request, 'gallery.html', context)

def contact(request):
    context = {"check" :3}
    return render(request, 'contact.html', context)

def semua(request):
    jad = Sched.objects.all()
    context = {"schedule": jad}
    return render(request, 'semua.html', context)

def haltambah(request):
    form = Jadwal()
    print("sad")
    if form.is_valid and request.method ==  "POST":
        form = Jadwal(request.POST)
        form.save()
        print("sukses")
        return redirect('/semua')
    else:
        context = {'form': form}
        return render(request, 'haltambah.html', context)

def tambah(request):
    fr = Jadwal()
    if fr.is_valid() and request.method ==  "POST":
        fr = Jadwal(request.POST)
        fr.save()
        return redirect('/semua')
    

def hapus(request, pk):
    jadw = Sched.objects.get(id = pk)
    jadw.delete()
    jad = Sched.objects.all()
    context = {"schedule": jad}
    return render(request, 'semua.html', context)

def detail(request, pk):
    ja = Sched.objects.all().filter(id=pk)
    context = {"schedule" : ja}
    return render(request, 'detail.html', context)
        
        