from django.db import models

# Create your models here.
class Sched(models.Model):
    namaMatkul = models.CharField(max_length = 50)
    dosen = models.CharField(max_length = 50)
    sks = models.CharField(max_length = 50)
    deskripsi = models.CharField(max_length = 50)
    tahun = models.CharField(max_length = 50)
    ruang = models.CharField(max_length = 50)