from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('exp/', views.exp, name='exp'),
    path('gallery/', views.gallery, name='gallery'),
    path('contact/', views.contact, name='contact'),
    path('semua/', views.semua, name='semua'),
    path('haltambah/', views.haltambah, name='haltambah'),
    path('tambah/', views.tambah, name="tambah"),
    path('semua/<int:pk>', views.hapus, name="hapus"),
    path('detail/<int:pk>', views.detail, name="detail"),
]